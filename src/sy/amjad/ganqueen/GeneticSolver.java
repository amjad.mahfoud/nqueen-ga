/**
 * Copyright 2014 Amjad Y. Mahfoud
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sy.amjad.ganqueen;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Amjad Y. Mahfoud <amjadoof@gmail.com>
 * 
 */
public class GeneticSolver {

	private int numGenes;

	/**
	 * Number of population per generation
	 */
	private int popNum;
	/**
	 * Number of iterations
	 */
	private int iter;

	/**
	 * The probability of a mutation to happen
	 */
	private double mutationProp;
	/**
	 * The percentage of elite in each generation
	 */
	private double elitePercentage;

	/**
	 * Chromosomes in each generation
	 */
	private Chromosome[] pop;

	public GeneticSolver(int numGenes) {
		this.numGenes = numGenes;
		this.popNum = 1000;
		this.iter = 1000;
		this.mutationProp = 0.01;
		this.elitePercentage = 0.5;
		this.pop = new Chromosome[popNum];
	}

	public GeneticSolver(int numGenes, int popNum, int iter,
			double mutationProp, double elitePercentage) {
		this.numGenes = numGenes;
		this.popNum = popNum;
		this.iter = iter;
		this.mutationProp = mutationProp;
		this.elitePercentage = elitePercentage;
		this.pop = new Chromosome[popNum];
	}

	/**
	 * Generate generations
	 */
	public void generate() {
		for (int i = 0; i < popNum; i++) {
			pop[i] = new Chromosome(this.numGenes);
		}
		Arrays.sort(pop);
	}

	/**
	 * Perform cross between chromosomes in the generation
	 */
	public void cross() {
		// TODO sumaia
		int it = 0;
		int start = (int) Math.round(popNum * elitePercentage);
		int end = (int) Math.round((2 * popNum * elitePercentage));

		while ((it < iter) && (pop[0].getFitness() != 0)) {
			int first = 0;
			int second = 1;
			for (int j = start; j < end - 1; j += 2) {
				Chromosome[] newch = new Chromosome[2];

				// int rr = (int) (Math.random() * numGenes);

				// newch = pop[first].crossInOnePoint(pop[second], rr);
				newch = pop[first].crossInTwoPoints(pop[second]);
				pop[j] = newch[0];
				pop[j + 1] = newch[1];
				first += 1;
				second += 1;
			}

			for (int cc = 0; cc < popNum; cc++) {
				for (int cc2 = 0; cc2 < 10; cc2++) {
					Random r = new Random();
					double rr = r.nextDouble();
					// System.err.println("mutation"+rr);
					if (rr <= mutationProp) {
						// System.err.println("mutation happend");
						pop[cc].mutate();
					}
				}
			}

			for (int cal = 0; cal < popNum; cal++)
				pop[cal].calcFitness();
			Arrays.sort(pop);
			it++;
		}
		if (pop[0].getFitness() == 0) {
			System.out
					.println("Optimal Solution is found in generation number: "
							+ it);
		} else {
			System.out
					.println("The optimal solution isn't found, try to change some attributes!");
		}
		System.out.println(pop[0]);
		System.out.println("\nBoard form: ");
		pop[0].printMatrix();
	}

	/**
	 * @return the popNum
	 */
	public int getPopNum() {
		return popNum;
	}

	/**
	 * @param popNum
	 *            the popNum to set
	 */
	public void setPopNum(int popNum) {
		this.popNum = popNum;
	}

	/**
	 * @return the iter
	 */
	public int getIter() {
		return iter;
	}

	/**
	 * @param iter
	 *            the iter to set
	 */
	public void setIter(int iter) {
		this.iter = iter;
	}

	/**
	 * @return the mutationProp
	 */
	public double getMutationProp() {
		return mutationProp;
	}

	/**
	 * @param mutationProp
	 *            the mutationProp to set
	 */
	public void setMutationProp(double mutationProp) {
		this.mutationProp = mutationProp;
	}

	/**
	 * @return the elitePercentage
	 */
	public double getElitePercentage() {
		return elitePercentage;
	}

	/**
	 * @param elitePercentage
	 *            the elitePercentage to set
	 */
	public void setElitePercentage(double elitePercentage) {
		this.elitePercentage = elitePercentage;
	}
}
