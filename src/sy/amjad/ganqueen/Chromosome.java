/**
 * Copyright 2014 Amjad Y. Mahfoud
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sy.amjad.ganqueen;

import java.util.Arrays;

/**
 * @author Amjad Y. Mahfoud <amjadoof@gmail.com>
 * 
 */
public class Chromosome implements Comparable<Chromosome> {

	/**
	 * number of genes in each chromosome
	 */
	private int numGenes;

	/**
	 * the fitness of each chromosome
	 */
	private int fitness = 0;

	/**
	 * the genetic series for the chromosome
	 */
	private int[] genes;

	/**
	 * 
	 */
	private double mutationMaxPer;

	/**
	 * Construct the chromosome with a specific number of genes
	 */
	public Chromosome(int numGenes) {
		this.numGenes = numGenes;
		this.genes = new int[this.numGenes];
		this.mutationMaxPer = 0.25;
		this.randomize();
		this.calcFitness();
	}

	public Chromosome(int numGenes, double mutationMaxPer) {
		this.numGenes = numGenes;
		this.genes = new int[this.numGenes];
		this.mutationMaxPer = mutationMaxPer;
		this.randomize();
		this.calcFitness();
	}

	/**
	 * Randomize the genes of the chromosome with values in the range
	 * [1,numGenes]
	 */
	private void randomize() {
		for (int i = 0; i < numGenes; i++)
			genes[i] = (int) (numGenes * Math.random() + 1);
	}

	/**
	 * Calculate the fitness value of the chromosome
	 */
	public void calcFitness() {
		fitness = 0;
		for (int i = 0; i < numGenes; i++) {
			for (int j = i + 1; j < numGenes; j++) {
				if (genes[j] == genes[i]
						|| Math.abs(i - j) == Math.abs(genes[j] - genes[i]))
					fitness++;
			}
		}
	}

	/**
	 * @return the number of genes for the chromosome
	 */
	public int getNumGenes() {
		return numGenes;
	}

	/**
	 * @return the fitness value of the chromosome
	 */
	public int getFitness() {
		return fitness;
	}

	@Override
	public int compareTo(Chromosome chromo) {
		return Double.compare(this.fitness, chromo.getFitness());
	}

	/**
	 * Randomly mutate the chromosome in randomly chosen genes
	 */
	public void mutate() {
		// choose the maximum number of genes to be mutated
		// Chosen be me to be less than the quarter
		int max = (int) ((numGenes * mutationMaxPer) * Math.random());
		// System.out.println("\tMax to be mutated: " + max);
		for (int i = 0; i < max; i++) {
			// choose a random gene to mutate
			int idx = (int) (numGenes * Math.random());
			// System.out.println("\tTo be mutated: " + idx + " val: " +
			// genes[idx]);
			// choose the new gene value randomly
			int newVal = (int) (numGenes * Math.random() + 1);
			// System.out.println("\tNew val: " + newVal);
			// mutate
			this.genes[idx] = newVal;
		}
		this.calcFitness();
	}

	/**
	 * Mutate the chromosome in a specific gene
	 * 
	 * @param mutationIdx
	 *            index of gene to be mutated
	 * @param newVal
	 *            new value to be inserted
	 */
	public void mutate(int mutationIdx, int newVal) {
		this.genes[mutationIdx] = newVal;
		this.calcFitness();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fitness;
		result = prime * result + Arrays.hashCode(genes);
		result = prime * result + numGenes;
		return result;
	}

	@Override
	public String toString() {
		return "Chromosome [fitness=" + fitness + ", genes="
				+ Arrays.toString(genes) + "]";
	}

	public Chromosome[] crossInOnePoint(Chromosome c, int pos) {		
		Chromosome cnew1 = new Chromosome(numGenes);
		Chromosome cnew2 = new Chromosome(numGenes);
		Chromosome[] cm = new Chromosome[2];
		for (int i = 0; i < pos; i++) {
			cnew1.genes[i] = this.genes[i];
			cnew2.genes[i] = c.genes[i];
		}
		for (int i = pos; i < numGenes; i++) {
			cnew1.genes[i] = c.genes[i];
			cnew2.genes[i] = this.genes[i];
		}
		cm[0] = cnew1;
		cm[1] = cnew2;
		return cm;
	}

	public Chromosome[] crossInTwoPoints(Chromosome c) {
		int crossStartIdx = (int) (Math.random() * numGenes);
		int crossEndIdx = (int) (Math.random() * numGenes);

		if (crossStartIdx < crossEndIdx) {
			int temp = crossEndIdx;
			crossEndIdx = crossStartIdx;
			crossStartIdx = temp;
		}

		Chromosome c1 = new Chromosome(numGenes);
		Chromosome c2 = new Chromosome(numGenes);

		Chromosome[] cm = new Chromosome[2];
		for (int i = 0; i < crossStartIdx; i++) {
			c1.genes[i] = this.genes[i];
			c2.genes[i] = c.genes[i];
		}

		for (int i = crossStartIdx; i < crossEndIdx; i++) {
			c1.genes[i] = c.genes[i];
			c2.genes[i] = this.genes[i];
		}

		for (int i = crossEndIdx; i < numGenes; i++) {
			c1.genes[i] = this.genes[i];
			c2.genes[i] = c.genes[i];
		}

		cm[0] = c1;
		cm[1] = c2;
		return cm;
	}

	public void printMatrix() {
		char[][] mat = new char[numGenes][numGenes];
		for (int i = 0; i < numGenes; i++) {
			for (int j = 0; j < numGenes; j++) {
				mat[i][j] = '-';
			}
		}

		for (int i = 0; i < numGenes; i++) {
			mat[genes[i] - 1][i] = 'Q';
		}

		for (int i = 0; i < numGenes; i++) {
			for (int j = 0; j < numGenes; j++) {
				System.out.print(mat[i][j] + " ");
			}
			System.out.println();
		}
	}
}
